define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'orders/task/index',
                    add_url: 'orders/task/add',
                    edit_url: 'orders/task/edit',
                    del_url: 'orders/task/del',
                    multi_url: 'orders/task/multi',
                    table: 'task',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'task.id',
                commonSearch: false,
                columns: [
                    [
                        { checkbox: true },
                        { field: 'id', title: __('Id'), operate: false, sortable: true },
                        { field: 'ordernumber', title: __('Ordernumber'), operate: 'LIKE' },
                        {
                            field: 'documenttype', title: __('Documenttype'), searchList: { 0: __('港澳居民来往内地通行证'), 1: __('台湾居民来往大陆通行证'), 2: __('往来港澳通行证'), 3: __('护照') }, formatter: function (value, row, index) {
                                if (row.documenttype == 0) {
                                    return '港澳居民来往内地通行证';
                                } else if (row.documenttype == 1) {
                                    return '台湾居民来往大陆通行证';
                                } else if (row.documenttype == 2) {
                                    return '往来港澳通行证';
                                } else if (row.documenttype == 3) {
                                    return '护照';
                                }
                            }
                        },
                        { field: 'identificationnumber', title: __('Reservationaccount'), operate: 'LIKE' },
                        { field: 'reservationpassword', title: __('Reservationpassword'), operate: 'LIKE' },
                        {
                            field: 'login_code_path', title: __('Imagecode'), operate: 'LIKE', formatter: function (value, row, index) {
                                if (row.login_code_path == '' || row.login_code_path === null) {
                                    return '';
                                } else {
                                    return '<img src="/' + row.login_code_path + '"></img>';
                                }

                            }
                        },
                        { field: 'login_code', title: __('Textcode'), operate: 'LIKE' },
                        {
                            field: 'status', title: __('Currentstatus'), operate: 'LIKE', formatter: function (value, row, index) {
                                if (row.status == 0) {
                                    return '等待执行';
                                } else if (row.status == 1) {
                                    return '启动中';
                                } else if (row.status == 1) {
                                    return '启动中';
                                } else if (row.status == 2) {
                                    return '登录中';
                                } else if (row.status == 3) {
                                    return '登录成功';
                                } else if (row.status == 4) {
                                    return '抢票中';
                                } else if (row.status == 90) {
                                    return '抢票失败';
                                } else if (row.status == 100) {
                                    return '抢票成功';
                                }
                            }
                        },
                        { field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate }
                        // {
                        //     field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: function (value, row, index) {
                        //         return '<a href="/back.php/orders/task/edit/ids/' + row.id + '" class="btn btn-xs btn-success btn-editone" data-toggle="tooltip" title="" data-table-id="table" data-field-index="13" data-row-index="' + index + '" data-button-index="1" data-original-title="输入验证码" data-area=["800px","350px"]><i class="fa fa-pencil"></i></a>';
                        //     }
                        // }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});