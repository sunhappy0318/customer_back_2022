define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'orders/orders/index',
                    add_url: 'orders/orders/add',
                    edit_url: 'orders/orders/edit',
                    del_url: 'orders/orders/del',
                    multi_url: 'orders/orders/multi',
                    table: 'orders',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'orders.id',
                columns: [
                    [
                        { checkbox: true },
                        { field: 'id', title: __('Id'), operate: false, sortable: true },
                        // { field: 'userid', title: __('Userid'), sortable: true },
                        { field: 'loginaccount', title: __('Loginaccount'), operate: 'LIKE' },
                        { field: 'ordernumber', title: __('Ordernumber'), operate: 'LIKE' },
                        {
                            field: 'documenttype', title: __('Documenttype'), searchList: { 0: __('港澳居民来往内地通行证'), 1: __('台湾居民来往大陆通行证'), 2: __('往来港澳通行证'), 3: __('护照') }, formatter: function (value, row, index) {
                                if (row.documenttype == 0) {
                                    return '港澳居民来往内地通行证';
                                } else if (row.documenttype == 1) {
                                    return '台湾居民来往大陆通行证';
                                } else if (row.documenttype == 2) {
                                    return '往来港澳通行证';
                                } else if (row.documenttype == 3) {
                                    return '护照';
                                }
                            }
                        },
                        { field: 'identificationnumber', title: __('Identificationnumber'), operate: 'LIKE' },
                        { field: 'reservationpassword', title: __('Reservationpassword'), operate: 'LIKE' },
                        { field: 'businesspersonnel', title: __('Businesspersonnel'), operate: 'LIKE' },
                        { field: 'orderamount', title: __('Orderamount'), operate: false, sortable: true },
                        // { field: 'createtime', title: __('Createtime'), formatter: Table.api.formatter.datetime, operate: false, addclass: 'datetimerange', sortable: true },
                        {
                            field: 'ispay', title: __('Ispay'), searchList: { 1: __('Yes'), 0: __('No') }, formatter: function (value, row, index) {
                                if (row.ispay == 0) {
                                    return '否';
                                } else if (row.ispay == 1) {
                                    return '是';
                                }
                            }
                        },
                        {
                            field: 'isappointmentsuccessful', title: __('Isappointmentsuccessful'), searchList: { 1: __('Yes'), 0: __('No') }, formatter: function (value, row, index) {
                                if (row.isappointmentsuccessful == 0) {
                                    return '否';
                                } else if (row.isappointmentsuccessful == 1) {
                                    return '是';
                                }
                            }
                        },
                        {
                            field: 'isstartappointment', title: __('Isstartappointment'), searchList: { 0: __('未开始'), 1: __('已开始'), 2: __('已结束') }, formatter: function (value, row, index) {
                                if (row.isstartappointment == 0) {
                                    return '未开始';
                                } else if (row.isstartappointment == 1) {
                                    return '已开始';
                                } else if (row.isstartappointment == 2) {
                                    return '已结束';
                                }
                            }
                        },
                        { field: 'updatetime', title: __('Updatetime'), formatter: Table.api.formatter.datetime, operate: false, addclass: 'datetimerange', sortable: true },
                        { field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});