<?php

namespace app\admin\model;

use think\Model;
use think\Db;

class Business extends Model
{

    // 表名
    protected $name = 'business';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    // 追加属性
    protected $append = [];

    public function getOriginData()
    {
        return $this->origin;
    }

    protected static function init()
    {
        self::beforeUpdate(function ($row) {
        });


        self::beforeUpdate(function ($row) {
        });
    }


    public static function getBusinessList()
    {
        $businessList = Db::name('business')->field('id,name')->select();
        return $businessList;
    }
}
