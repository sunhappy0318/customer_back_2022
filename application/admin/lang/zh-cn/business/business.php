<?php

return [
    'Id'          => 'ID',
    'Name'        => '姓名',
    'Code'        => '分销二维码',
    'Website'     => '分销链接',
    'Createtime'  => '创建时间',
    'Updatetime'  => '更新时间'
];
