<?php

return [
    'Id'                      => 'ID',
    'Userid'                  => '账户id',
    'Loginaccount'            => '登录账户',
    'Ordernumber'             => '订单编号',
    'Documenttype'            => '证件类型',
    'Identificationnumber'    => '证件号码',
    'Reservationpassword'     => '预约密码',
    'Orderamount'             => '订单金额',
    'Businesspersonnel'       => '业务人员',
    'Createtime'              => '创建时间',
    'Updatetime'              => '更新时间',
    'Ispay'                   => '是否付费',
    'Isappointmentsuccessful' => '是否预约成功',
    'Isstartappointment'      => '是否开始预约',
    'Yes'                     => '是',
    'No'                      => '否',
];
