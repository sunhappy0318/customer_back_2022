<?php

return [
    'Id'                      => 'ID',
    'Ordernumber'             => '订单编号',
    'Reservationaccount'      => '预约账号',
    'Reservationpassword'     => '预约密码',
    'Createtime'              => '创建时间',
    'Updatetime'              => '更新时间',
    'Documenttype'            => '证件类型',
    'Currentstatus'           => '当前状态',
    'Imagecode'               => '图片验证码',
    'Textcode'                => '文本输入',
];
