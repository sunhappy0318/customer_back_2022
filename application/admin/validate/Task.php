<?php

namespace app\admin\validate;

use think\Validate;

class Task extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'login_code' => 'require',
    ];

    /**
     * 字段描述
     */
    protected $field = [];
    /**
     * 提示消息
     */
    protected $message = [];
    /**
     * 验证场景
     */
    protected $scene = [
        'edit' => ['login_code'],
    ];

    public function __construct(array $rules = [], $message = [], $field = [])
    {
        $this->field = [
            'login_code' => __('Textcode')
        ];
        parent::__construct($rules, $message, $field);
    }
}
