<?php

namespace app\admin\validate;

use think\Validate;

class Orders extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'identificationnumber' => 'require|unique:orders,identificationnumber^reservationpassword',
        'reservationpassword' => 'require',
    ];

    /**
     * 字段描述
     */
    protected $field = [];
    /**
     * 提示消息
     */
    protected $message = [];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => ['identificationnumber',  'reservationpassword'],
        'edit' => ['identificationnumber',  'reservationpassword'],
    ];

    public function __construct(array $rules = [], $message = [], $field = [])
    {
        $this->field = [
            'identificationnumber' => __('Identificationnumber'),
            'reservationpassword'   => __('Reservationpassword')
        ];
        parent::__construct($rules, $message, $field);
    }
}
