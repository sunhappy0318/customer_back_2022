<?php

namespace app\admin\controller\orders;

use app\common\controller\Backend;
use app\common\library\Auth;
use think\Db;

/**
 * 预约页面
 *
 * @icon fa fa-user
 */
class Task extends Backend
{

    protected $relationSearch = true;
    protected $searchFields = 'id,login_code';

    /**
     * @var \app\admin\model\Orders
     */
    protected $model = null;
    protected $modelValidate = true;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('Task');
    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            foreach ($list as $key => &$val) {
                $ordersInfo = Db::name('Orders')->where("id=" . $val['order_id'])->find();
                $val['ordernumber'] = $ordersInfo['ordernumber'];
                $val['documenttype'] = $ordersInfo['documenttype'];
                $val['identificationnumber'] = $ordersInfo['identificationnumber'];
                $val['reservationpassword'] = $ordersInfo['reservationpassword'];
            }
            unset($val);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            $this->token();
        }
        $this->modelValidate = true;
        return parent::edit($ids);
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        $row = $this->model->get($ids);
        $this->modelValidate = true;
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($ids) {
            Db::startTrans();
            try {
                $this->model->destroy($ids);
                Db::commit();
            } catch (\Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            $this->success();
        }
        $this->error(__('No rows were deleted'));
    }
}
