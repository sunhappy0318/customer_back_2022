<?php

namespace app\admin\controller\orders;

use app\common\controller\Backend;
use app\common\library\Auth;
use think\Db;

/**
 * 订单管理
 *
 * @icon fa fa-user
 */
class Orders extends Backend
{

    protected $relationSearch = true;
    protected $searchFields = 'id,ordernumber,identificationnumber,businesspersonnel';

    /**
     * @var \app\admin\model\Orders
     */
    protected $model = null;
    protected $reservationmodel = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('Orders');
        $this->userList = \app\admin\model\User::getUserList();
        $this->businessList = \app\admin\model\Business::getBusinessList();
        $this->typeList = ['0' => '港澳居民来往内地通行证', '1' => '台湾居民来往大陆通行证', '2' => '往来港澳通行证', '3' => '护照'];
    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $this->token();
        }
        $this->modelValidate = true;
        $this->view->assign('userList', $this->userList);
        $this->view->assign('typeList', build_select('row[documenttype]', $this->typeList, ['class' => 'form-control selectpicker']));
        $this->view->assign('businessList', $this->businessList);
        return parent::add();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            $this->token();
        }

        $this->modelValidate = true;
        $this->view->assign('userList', $this->userList);
        $this->view->assign('typeList', build_select('row[documenttype]', $this->typeList, $row['documenttype'], ['class' => 'form-control selectpicker']));
        $this->view->assign('businessList', $this->businessList);
        return parent::edit($ids);
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        $row = $this->model->get($ids);
        $this->modelValidate = true;
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($ids) {
            Db::startTrans();
            try {
                $this->model->destroy($ids);
                Db::commit();
            } catch (\Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            $this->success();
        }
        $this->error(__('No rows were deleted'));
    }
}
